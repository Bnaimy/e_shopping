<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cartebancaire
 *
 * @ORM\Table(name="cartebancaire")
 * @ORM\Entity
 */
class Cartebancaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_carte", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCarte;

    /**
     * @var string
     *
     * @ORM\Column(name="type_carte", type="string", length=255, nullable=false)
     */
    private $typeCarte;

    /**
     * @var int
     *
     * @ORM\Column(name="csv_code", type="integer", nullable=false)
     */
    private $csvCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefincarte", type="date", nullable=false)
     */
    private $datefincarte;


}
