<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Admin
 *
 * @ORM\Table(name="admin", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_880E0D762AA521C9", columns={"numero_tele"}), @ORM\UniqueConstraint(name="UNIQ_880E0D76AA08CB10", columns={"login"})})
 * @ORM\Entity
 */
class Admin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_admin", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=10, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=10, nullable=false)
     */
    private $password;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_tele", type="integer", nullable=false)
     */
    private $numeroTele;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Client", inversedBy="adminIdAdmin")
     * @ORM\JoinTable(name="gerer",
     *   joinColumns={
     *     @ORM\JoinColumn(name="admin_id_admin", referencedColumnName="id_admin")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="client_id_client", referencedColumnName="id_client")
     *   }
     * )
     */
    private $clientIdClient;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientIdClient = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
