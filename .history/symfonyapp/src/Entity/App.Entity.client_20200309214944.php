<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class client
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_client;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $login;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $password;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $addressepostal;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $pays;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $ville;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $codepostal;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $datenaissance;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\cartebancaire", inversedBy="clients")
     * @ORM\JoinColumn(name="carte_bancaire_id_carte", referencedColumnName="id_carte", unique=true)
     */
    private $carte bancaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\bondecommande", inversedBy="clients")
     * @ORM\JoinColumn(name="bondecommande_id_commande", referencedColumnName="idcommande")
     */
    private $bondecommande;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\article", mappedBy="clients")
     */
    private $articles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\admin", mappedBy="clients")
     */
    private $admins;
}