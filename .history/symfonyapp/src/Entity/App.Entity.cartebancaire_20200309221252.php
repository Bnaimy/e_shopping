<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class cartebancaire
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_carte;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $type_carte;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $csv_code;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $datefincarte;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\client", mappedBy="carte bancaire")
     */
    private $clients;
}