<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 */
class admin
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=4)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_admin;

    /**
     * @ORM\Column(type="string", unique=true, length=10, nullable=false)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    private $password;

    /**
     * @ORM\Column(type="integer", unique=true, length=10, nullable=false)
     */
    private $numero_tele;

    /**
     * @ORM\ManyToMany(targetEntity="Client", inversedBy="Admins")
     * @ORM\JoinTable(
     *     name="gerer",
     *     joinColumns={@ORM\JoinColumn(name="admin_id_admin", referencedColumnName="id_admin", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="client_id_client", referencedColumnName="id_client", nullable=false)}
     * )
     */
    private $Clients;
}