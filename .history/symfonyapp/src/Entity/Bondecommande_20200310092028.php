<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bondecommande
 *
 * @ORM\Table(name="bondecommande")
 * @ORM\Entity
 */
class Bondecommande
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcommande", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcommande;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true, options={"default"="NULL"})
     */
    private $date = 'NULL';


}
