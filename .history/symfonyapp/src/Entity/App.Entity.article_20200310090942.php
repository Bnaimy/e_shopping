<?php

namespace App\Entity;
/**
 * @ORM\Entity(repositoryClass="App\Repository\MyClassRepository")
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class article
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_article;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $prix_unitaire;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $marque;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\produit", inversedBy="articles")
     * @ORM\JoinColumn(name="produit_id_produit", referencedColumnName="id_produit")
     */
    private $produit;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\client", inversedBy="articles")
     * @ORM\JoinTable(
     *     name="panier",
     *     joinColumns={@ORM\JoinColumn(name="article_id_article", referencedColumnName="id_article", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="client_id_client", referencedColumnName="id_client", nullable=false)}
     * )
     */
    private $clients;
}