<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=4)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_client;

    /**
     * @ORM\ManyToMany(targetEntity="admin", mappedBy="Clients")
     */
    private $Admins;
}