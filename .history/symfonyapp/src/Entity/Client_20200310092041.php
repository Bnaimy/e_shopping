<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_client", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idClient;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Admin", mappedBy="clientIdClient")
     */
    private $adminIdAdmin;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->adminIdAdmin = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
