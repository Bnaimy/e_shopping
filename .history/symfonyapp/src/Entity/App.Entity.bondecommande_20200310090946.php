<?php

namespace App\Entity;
/**
 * @ORM\Entity(repositoryClass="App\Repository\MyClassRepository")
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class bondecommande
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idcommande;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\client", mappedBy="bondecommande")
     */
    private $clients;
}