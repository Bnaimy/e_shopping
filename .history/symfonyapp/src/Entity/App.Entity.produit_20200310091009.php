<?php

namespace App\Entity;
/**
 * @ORM\Entity(repositoryClass="App\Repository\MyClassRepository")
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class produit
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_produit;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\article", mappedBy="produit")
     */
    private $articles;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\categorie", inversedBy="produits")
     * @ORM\JoinColumn(name="categorie_id_categorie", referencedColumnName="id_categorie")
     */
    private $categorie;
}