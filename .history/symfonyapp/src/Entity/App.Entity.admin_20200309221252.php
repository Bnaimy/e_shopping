<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class admin
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $login;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $password;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $numero_tele;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\client", inversedBy="admins")
     * @ORM\JoinTable(
     *     name="gestion_client",
     *     joinColumns={@ORM\JoinColumn(name="admin_id", referencedColumnName="id", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="client_id_client", referencedColumnName="id_client", nullable=false)}
     * )
     */
    private $clients;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\categorie", mappedBy="admins")
     */
    private $categories;
}