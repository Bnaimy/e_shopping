<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Produit
 *
 * @ORM\Table(name="produit", indexes={@ORM\Index(name="IDX_29A5EC27B04B2C91", columns={"categorie_id_categorie"})})
 * @ORM\Entity(repositoryClass="App\Repository\produitRepository")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_produit", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProduit;

    /**
     * @var int|null
     *
     * @ORM\Column(name="categorie_id_categorie", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $categorieIdCategorie = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    public function getIdProduit(): ?int
    {
        return $this->idProduit;
    }

    public function getCategorieIdCategorie(): ?int
    {
        return $this->categorieIdCategorie;
    }

    public function setCategorieIdCategorie(?int $categorieIdCategorie): self
    {
        $this->categorieIdCategorie = $categorieIdCategorie;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


}
