<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * pANIER
 *
 * @ORM\Table(name="Panier")
 * @ORM\Entity(repositoryClass="App\Repository\PanierRepository")
 */

/**
 * Panier
 *
 * @ORM\Table(name="panier", indexes={@ORM\Index(name="IDX_24CC0DF2E07E7697", columns={"article_id_article"}), @ORM\Index(name="IDX_24CC0DF2A5361B7", columns={"client_id_client"})})
 * @ORM\Entity
 */
class Panier
{
    /**
     * @var int
     *
     * @ORM\Column(name="article_id_article", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $articleIdArticle;

    /**
     * @var int
     *
     * @ORM\Column(name="client_id_client", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $clientIdClient;

    public function getArticleIdArticle(): ?int
    {
        return $this->articleIdArticle;
    }

    public function getClientIdClient(): ?int
    {
        return $this->clientIdClient;
    }


}
