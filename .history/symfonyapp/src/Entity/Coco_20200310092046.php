<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coco
 *
 * @ORM\Table(name="coco")
 * @ORM\Entity
 */
class Coco
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="a", type="integer", nullable=false)
     */
    private $a;


}
