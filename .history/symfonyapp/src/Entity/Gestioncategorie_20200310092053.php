<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gestioncategorie
 *
 * @ORM\Table(name="gestioncategorie", indexes={@ORM\Index(name="IDX_A374877D642B8210", columns={"admin_id"}), @ORM\Index(name="IDX_A374877DB04B2C91", columns={"categorie_id_categorie"})})
 * @ORM\Entity
 */
class Gestioncategorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $adminId;

    /**
     * @var int
     *
     * @ORM\Column(name="categorie_id_categorie", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $categorieIdCategorie;


}
