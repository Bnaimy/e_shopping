<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Cartebancaire
 *
 * @ORM\Table(name="Cartebancaire")
 * @ORM\Entity(repositoryClass="App\Repository\CartebancaireRepository")
 */

/**
 * Cartebancaire
 *
 * @ORM\Table(name="cartebancaire")
 * @ORM\Entity
 */
class Cartebancaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_carte", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCarte;

    /**
     * @var string
     *
     * @ORM\Column(name="type_carte", type="string", length=255, nullable=false)
     */
    private $typeCarte;

    /**
     * @var int
     *
     * @ORM\Column(name="csv_code", type="integer", nullable=false)
     */
    private $csvCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefincarte", type="date", nullable=false)
     */
    private $datefincarte;

    public function getIdCarte(): ?int
    {
        return $this->idCarte;
    }

    public function getTypeCarte(): ?string
    {
        return $this->typeCarte;
    }

    public function setTypeCarte(string $typeCarte): self
    {
        $this->typeCarte = $typeCarte;

        return $this;
    }

    public function getCsvCode(): ?int
    {
        return $this->csvCode;
    }

    public function setCsvCode(int $csvCode): self
    {
        $this->csvCode = $csvCode;

        return $this;
    }

    public function getDatefincarte(): ?\DateTimeInterface
    {
        return $this->datefincarte;
    }

    public function setDatefincarte(\DateTimeInterface $datefincarte): self
    {
        $this->datefincarte = $datefincarte;

        return $this;
    }


}
