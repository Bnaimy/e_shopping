<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class categorie
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_categorie;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\produit", mappedBy="categorie")
     */
    private $produits;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\admin", inversedBy="categories")
     * @ORM\JoinTable(
     *     name="gescategorie",
     *     joinColumns={@ORM\JoinColumn(name="categorie_id_categorie", referencedColumnName="id_categorie", nullable=false)},
     *     inverseJoinColumns={@ORM\JoinColumn(name="admin_id", referencedColumnName="id", nullable=false)}
     * )
     */
    private $admins;
}