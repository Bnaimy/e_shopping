<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * GestionClient
 *
 * @ORM\Table(name="GestionClient")
 * @ORM\Entity(repositoryClass="App\Repository\GestionClientRepository")
 */
/**
 * GestionClient
 *
 * @ORM\Table(name="gestion_client", indexes={@ORM\Index(name="IDX_FE0AA8F8642B8210", columns={"admin_id"}), @ORM\Index(name="IDX_FE0AA8F8A5361B7", columns={"client_id_client"})})
 * @ORM\Entity
 */
class GestionClient
{
    /**
     * @var int
     *
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $adminId;

    /**
     * @var int
     *
     * @ORM\Column(name="client_id_client", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $clientIdClient;

    public function getAdminId(): ?int
    {
        return $this->adminId;
    }

    public function getClientIdClient(): ?int
    {
        return $this->clientIdClient;
    }


}
