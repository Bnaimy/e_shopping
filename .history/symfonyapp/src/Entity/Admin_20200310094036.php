<?php
/**
 * Admin
 *
 * @ORM\Entity(repositoryClass="\Repository\AdminRepository")
 */
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
class Admin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_admin", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=10, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=10, nullable=false)
     */
    private $password;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_tele", type="integer", nullable=false)
     */
    private $numeroTele;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Client", inversedBy="adminIdAdmin")
     * @ORM\JoinTable(name="gerer",
     *   joinColumns={
     *     @ORM\JoinColumn(name="admin_id_admin", referencedColumnName="id_admin")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="client_id_client", referencedColumnName="id_client")
     *   }
     * )
     */
    private $clientIdClient;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientIdClient = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdAdmin(): ?int
    {
        return $this->idAdmin;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getNumeroTele(): ?int
    {
        return $this->numeroTele;
    }

    public function setNumeroTele(int $numeroTele): self
    {
        $this->numeroTele = $numeroTele;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClientIdClient(): Collection
    {
        return $this->clientIdClient;
    }

    public function addClientIdClient(Client $clientIdClient): self
    {
        if (!$this->clientIdClient->contains($clientIdClient)) {
            $this->clientIdClient[] = $clientIdClient;
        }

        return $this;
    }

    public function removeClientIdClient(Client $clientIdClient): self
    {
        if ($this->clientIdClient->contains($clientIdClient)) {
            $this->clientIdClient->removeElement($clientIdClient);
        }

        return $this;
    }

}
