<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Client
{
    /**
     * @ORM\Column(nullable=true)
     */
    private $id_client;

    /**
     * @ORM\ManyToMany(targetEntity="admin", mappedBy="Clients")
     */
    private $Admins;
}