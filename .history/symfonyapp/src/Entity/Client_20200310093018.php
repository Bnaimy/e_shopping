<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**
 * Client
 *
 * @ORM\Table(name="Client")
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_client", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idClient;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Admin", mappedBy="clientIdClient")
     */
    private $adminIdAdmin;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->adminIdAdmin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdClient(): ?int
    {
        return $this->idClient;
    }

    /**
     * @return Collection|Admin[]
     */
    public function getAdminIdAdmin(): Collection
    {
        return $this->adminIdAdmin;
    }

    public function addAdminIdAdmin(Admin $adminIdAdmin): self
    {
        if (!$this->adminIdAdmin->contains($adminIdAdmin)) {
            $this->adminIdAdmin[] = $adminIdAdmin;
            $adminIdAdmin->addClientIdClient($this);
        }

        return $this;
    }

    public function removeAdminIdAdmin(Admin $adminIdAdmin): self
    {
        if ($this->adminIdAdmin->contains($adminIdAdmin)) {
            $this->adminIdAdmin->removeElement($adminIdAdmin);
            $adminIdAdmin->removeClientIdClient($this);
        }

        return $this;
    }

}
